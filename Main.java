public class Main {
    public static void main(String[] args) {
        Interviewer employer = new Interviewer();
        Candidate[] candidates = setCandidates();  // Polymorphism
        for (Candidate candidate : candidates) {
            employer.hello();
            candidate.hello();
            candidate.describeExperience();
        }
    }

    public static Candidate[] setCandidates() {    // Polymorphism
        Candidate[] candidates = new Candidate[10];
        candidates[0] = new SelfLearner("Stan");   // Polymorphism
        candidates[1] = new SelfLearner("Dipper");
        candidates[2] = new SelfLearner("Mabel");
        candidates[3] = new SelfLearner("Rick");
        candidates[4] = new SelfLearner("Ford");
        candidates[5] = new Graduate("Shelby");      // Polymorphism
        candidates[6] = new Graduate("Alexander");
        candidates[7] = new Graduate("Maksim");
        candidates[8] = new Graduate("Helga");
        candidates[9] = new Graduate("Michael");
        return candidates;
    }
}

class Interviewer {
    public void hello() {
        System.out.println("hi! introduce yourself and describe your java experience please");
    }
}

interface Candidate {
    void describeExperience();

    void hello();
}

abstract class Programmer implements Candidate {   // Abstract class provide common functionality of related classes
    private String name;
    public void hello() {
        System.out.println("hi! my name is " + name +"!");
    }
    Programmer (String name) {
        this.name = name;
    }
}

class SelfLearner extends Programmer {
    SelfLearner (String name) {
        super(name);
    }

    public void describeExperience() {
        System.out.println("I have been learning Java by myself, nobody examined how thorough is my knowledge and how good is my code.");
    }
}

class Graduate extends Programmer {
    Graduate (String name) {
        super(name);
    }

    public void describeExperience() {
        System.out.println("I passed successfully getJavaJob exams and code reviews.");
    }
}


/*
company "coolJavaProjects" holds an interview with 10 java candidates for java developer position.
        there are getJavaJob graduates and self-learners candidates in the list to be interviewed.
        in the interview employer salutes every candidate with "hi! introduce yourself and describe your java experience please"
        candidate replies with his name as "hi! my name is <name>!" and describes his java experience in next way:
        self-learner describes his experience as: "I have been learning Java by myself, nobody examined how thorough is my knowledge and how good is my code."
        and getJavaJob graduates say: "I passed successfully getJavaJob exams and code reviews."

        implement task as a console app that outputs dialog between employer and candidates with this code:
        for (Candidate candidate : candidates) {
        employer.hello();
        candidate.hello();
        candidate.describeExperience();
        }
        use interfaces and abstract classes in your solution.
        all OOP principles must be used in your solution. comment each of them in your code.
abstract class must contain some implementation, not just only marked with an abstract keyword or to have only single abstract method.
        for every type of candidate there should be a separate class.*/
